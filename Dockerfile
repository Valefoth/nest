###### BUILDER STAGE
FROM node AS builder
WORKDIR /build
COPY . .
RUN npm i -g @nestjs/cli
RUN npm install && nest build

###### IMAGE DEFINITION
FROM node:alpine
RUN apk --no-cache add ca-certificates
WORKDIR /api
COPY --from=builder --chown=1000:1000 /build/node_modules /api/node_modules
COPY --from=builder --chown=1000:1000 /build/dist /api/
COPY --from=builder --chown=1000:1000 /build/sqlite.db /api/
EXPOSE 3000
USER 1000:1000
CMD [ "node","./main" ]
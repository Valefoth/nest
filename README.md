# exo nest

Faire un blog avec 
- des posts
- author
- des commentaires 
- des tags
 

## Post
- Title 
- Subtitle
- Date
- Author (ref)
- Comments (ref) 
- Tags 
- images


## Comments
- author 
- post 
- date
- content 
- points (default 1) 


## Tags
- id
- nom

## Authors
- Nom, Prenom
- email
- Index/Commentaires 


## Methods
- CRUD all
- incrementPoint() 

## Description

[Nest](https://github.com/nestjs/nest) framework TypeScript starter repository.

## Installation

```bash
$ npm install
```

## Running the app

```bash
# development
$ npm run start

# watch mode
$ npm run start:dev

# production mode
$ npm run start:prod
```

## Test

```bash
# unit tests
$ npm run test

# e2e tests
$ npm run test:e2e

# test coverage
$ npm run test:cov
```




## SQL PART

- Telecharger sqlite shell
- sqlite3 generate.sql


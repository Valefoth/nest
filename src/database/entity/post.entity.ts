import {
  Entity,
  Column,
  OneToOne,
  JoinTable,
  ManyToMany,
  PrimaryGeneratedColumn,
  BaseEntity,
  ManyToOne,
} from 'typeorm';
import { AuthorEntity } from './author.entity';
import { TagEntity } from './tag.entity';

@Entity('POSTS')
export class PostsEntity extends BaseEntity {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({ length: 500 })
  title: string;

  @Column()
  subtitle: string;

  @Column()
  date: Date;

  @ManyToOne((type) => AuthorEntity, (AuthorEntity) => AuthorEntity.id)
  author: AuthorEntity;

  @Column()
  images: number;

  @ManyToMany(() => TagEntity, (TagEntity) => TagEntity.posts)
  @JoinTable()
  tags: TagEntity[];
}

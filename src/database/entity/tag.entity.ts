import {
  Entity,
  Column,
  PrimaryGeneratedColumn,
  BaseEntity,
  ManyToMany,
  JoinTable,
} from 'typeorm';
import { PostsEntity } from './post.entity';

@Entity('TAGS')
export class TagEntity extends BaseEntity {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  nom: string;

  @ManyToMany(() => PostsEntity, (PostsEntity) => PostsEntity.tags)
  @JoinTable()
  posts: PostsEntity[];
}

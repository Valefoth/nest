import {
  Entity,
  Column,
  PrimaryGeneratedColumn,
  BaseEntity,
  ManyToOne,
  OneToMany,
} from 'typeorm';
import { AuthorEntity } from './author.entity';
import { PostsEntity } from './post.entity';

@Entity('COMMENTS')
export class CommentsEntity extends BaseEntity {
  @PrimaryGeneratedColumn()
  id: number;

  @ManyToOne(() => AuthorEntity, (AuthorEntity) => AuthorEntity.id)
  author: AuthorEntity;

  @ManyToOne(() => PostsEntity, (PostsEntity) => PostsEntity.id)
  post: PostsEntity;

  @Column()
  date: Date;

  @Column()
  content: string;

  @Column()
  points: number;
}

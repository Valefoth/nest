import { Injectable } from '@nestjs/common';
import { AuthorEntity } from 'src/database/entity/author.entity';
import { PostsEntity } from 'src/database/entity/post.entity';
import { CreatePostsDto } from './dto/create-posts.dto';

@Injectable()
export class PostsService {
  async findAll(): Promise<PostsEntity[]>{
    return await PostsEntity.find({relations: ["author","tags"]});
  }

  async insert(posts: CreatePostsDto): Promise<PostsEntity> {
    const { id, subtitle, date, author, images, title } = posts;

    const postsEntity: PostsEntity = PostsEntity.create();

    postsEntity.subtitle = subtitle;
    postsEntity.date = date;
    postsEntity.title = title;
    postsEntity.author = author;
    postsEntity.images = images;
    await PostsEntity.save(postsEntity);
    return postsEntity;
  }

  async findOne(postId: number): Promise<PostsEntity> {
    const post: PostsEntity = await PostsEntity.findOne({
      where: { id: postId },
      relations: ['author'],
    });
    return post;
  }

  async delete(postId: number): Promise<PostsEntity> {
    const post: PostsEntity = await PostsEntity.findOne({
      where: { id: postId },
    });
    PostsEntity.delete(postId);
    return post;
  }
}

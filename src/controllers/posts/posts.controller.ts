import { Body, Controller, Delete, Get, Param, Put } from '@nestjs/common';
import { PostsService } from './posts.service';
import { CreatePostsDto } from './dto/create-posts.dto';

@Controller('posts')
export class PostsController {
  constructor(private readonly postsService: PostsService) {}

  @Get()
  findAll() {
    return this.postsService.findAll().then((res) => {
      return res;
    });
  }

  @Get(':id')
  async findOne(@Param() params) {
    return this.postsService.findOne(params.id).then((res) => res);
  }

  @Put()
  async postPost(@Body() post: CreatePostsDto) {
    return this.postsService.insert(post).then((res) => res);
  }

  @Delete(':id')
  async delete(@Param() params) {
    return this.postsService.delete(params.id).then((res) => res);
  }
}

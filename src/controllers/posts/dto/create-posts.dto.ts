import { AuthorEntity } from 'src/database/entity/author.entity';

export class CreatePostsDto {
  readonly id: number;

  readonly title: string;

  readonly subtitle: string;

  readonly date: Date;

  readonly author: AuthorEntity;

  readonly images: number;
}

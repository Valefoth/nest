import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { PostsModule } from '../posts/posts.module';
import { AuthorsModule } from '../authors/authors.module';
import { CommentsModule } from '../comments/comments.module';
import { TagsModule } from '../tags/tags.module';
@Module({
  imports: [
    TypeOrmModule.forRoot({
      type: 'sqlite',
      database: 'sqlite.db',
      entities: [__dirname + '/database/entity/*.entity{.ts,.js}'],
      synchronize: false,
      autoLoadEntities: true,
    }),
    PostsModule,
    AuthorsModule,
    CommentsModule,
    TagsModule,
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}

import { Body, Controller, Delete, Get, Param, Put } from '@nestjs/common';
import { TagsService } from './tags.service';
import { CreateTagDto } from './dto/create-tags.dto';

@Controller('tags')
export class TagsController {
  constructor(private readonly tagsService: TagsService) {}

  @Get()
  async findAll() {
    return this.tagsService.findAll().then((res) => {
      return res;
    });
  }

  @Get(':id')
  async findOne(@Param() params) {
    return this.tagsService.findOne(params.id).then((res) => {
      return res;
    });
  }

  @Put()
  async putTag(@Body() tag: CreateTagDto) {
    return this.tagsService.insert(tag).then((res) => {
      return res;
    });
  }

  @Delete(':id')
  async delete(@Param() params) {
    return this.tagsService.delete(params.id).then((res) => {
      return res;
    });
  }
}

import { Injectable } from '@nestjs/common';
import { TagEntity } from 'src/database/entity/tag.entity';
import { CreateTagDto } from './dto/create-tags.dto';

@Injectable()
export class TagsService {
  async findAll(): Promise<TagEntity[]> {
    return await TagEntity.find({ relations : ["posts"] });
  }

  async insert(author: CreateTagDto): Promise<TagEntity> {
    const { id, nom } = author;
    const authorEntity: TagEntity = TagEntity.create();
    if (id) authorEntity.id = id;
    authorEntity.nom = nom;
    await TagEntity.save(authorEntity);
    //await book.save();
    return authorEntity;
  }

  async findOne(id: number): Promise<TagEntity> {
    const author: TagEntity = await TagEntity.findOne({ where: { id: id } });
    return author;
  }

  async delete(id: number): Promise<TagEntity> {
    const author: TagEntity = await TagEntity.findOne({ where: { id: id } });
    TagEntity.delete(author);
    return author;
  }
}

import { Body, Controller, Delete, Get, Param, Put } from '@nestjs/common';
import { AuthorsService } from './authors.service';
import { CreateAuthorDto } from './dto/create-authors.dto';

@Controller('authors')
export class AuthorsController {
  constructor(private readonly authorsService: AuthorsService) {}

  @Get()
  async findAll() {
    return this.authorsService.findAll().then((res) => {
      return res;
    });
  }

  @Get(':id')
  async findOne(@Param() params) {
    return this.authorsService.findOne(params.id).then((res) => {
      return res;
    });
  }

  @Put()
  async putAuthor(@Body() author: CreateAuthorDto) {
    return this.authorsService.insert(author).then((res) => {
      return res;
    });
  }

  @Delete(':id')
  async delete(@Param() params) {
    return this.authorsService.delete(params.id).then((res) => {
      return res;
    });
  }
}

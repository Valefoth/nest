export class CreateAuthorDto {
  readonly id: number;

  readonly nom: string;

  readonly prenom: string;
}

import { Injectable } from '@nestjs/common';
import { AuthorEntity } from 'src/database/entity/author.entity';
import { CreateAuthorDto } from './dto/create-authors.dto';

@Injectable()
export class AuthorsService {
  async findAll(): Promise<AuthorEntity[]> {
    return await AuthorEntity.find();
  }

  async insert(author: CreateAuthorDto): Promise<AuthorEntity> {
    const { id, nom, prenom } = author;
    const authorEntity: AuthorEntity = AuthorEntity.create();
    if (id) authorEntity.id = id;
    authorEntity.nom = nom;
    authorEntity.prenom = prenom;
    await AuthorEntity.save(authorEntity);
    //await book.save();
    return authorEntity;
  }

  async findOne(id: number): Promise<AuthorEntity> {
    const author: AuthorEntity = await AuthorEntity.findOne({
      where: { id: id },
    });
    return author;
  }

  async delete(id: number): Promise<AuthorEntity> {
    const author: AuthorEntity = await AuthorEntity.findOne({
      where: { id: id },
    });
    AuthorEntity.delete(author);
    return author;
  }
}

import { Injectable } from '@nestjs/common';
import { CommentsEntity } from 'src/database/entity/comment.entity';
import { CreateCommentDto } from './dto/create-comments.dto';

@Injectable()
export class CommentsService {
  async findAll(): Promise<CommentsEntity[]> {
    return await CommentsEntity.find({ relations: ['author', 'post'] });
  }

  async insert(comment: CreateCommentDto): Promise<CommentsEntity> {
    const { id, author, post, date, content, points } = comment;
    const commentEntity: CommentsEntity = CommentsEntity.create();

    commentEntity.author = author;
    commentEntity.post = post;
    commentEntity.date = date;
    commentEntity.content = content;
    await CommentsEntity.save(commentEntity);
    return commentEntity;
  }

  async findOne(commentId: number): Promise<CommentsEntity> {
    const comment: CommentsEntity = await CommentsEntity.findOne({
      where: { id: commentId },
      relations: ['author', 'post'],
    });
    return comment;
  }

  async deleteOne(commentId: number): Promise<CommentsEntity> {
    const comment: CommentsEntity = await CommentsEntity.findOne({
      where: { id: commentId },
    });
    CommentsEntity.delete(commentId);
    return comment;
  }
}

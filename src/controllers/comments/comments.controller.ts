import { Body, Controller, Delete, Get, Param, Put } from '@nestjs/common';
import { CommentsService } from './comments.service';
import { CreateCommentDto } from './dto/create-comments.dto';

@Controller('comments')
export class CommentsController {
  constructor(private readonly commentsService: CommentsService) {}

  @Get()
  findAll() {
    return this.commentsService.findAll().then((res) => {
      return res;
    });
  }

  @Get(':id')
  findOne(@Param() params) {
    return this.commentsService.findOne(params.id).then((res) => res);
  }
  @Put()
  async insert(@Body() comment: CreateCommentDto) {
    return this.commentsService.insert(comment).then((res) => res);
  }

  @Delete(':id')
  deleteOne(@Param() params) {
    return this.commentsService.deleteOne(params.id).then((res) => res);
  }
}

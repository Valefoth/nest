import { AuthorEntity } from 'src/database/entity/author.entity';
import { PostsEntity } from 'src/database/entity/post.entity';

export class CreateCommentDto {
  readonly id: number;
  readonly author: AuthorEntity;
  readonly post: PostsEntity;
  readonly date: Date;
  readonly content: string;
  readonly points: number;
}

create table POSTS(
    id INTEGER PRIMARY KEY AUTOINCREMENT,
    title varchar(250) not null,
    subtitle varchar(250),
    date date not null,
    authorId INTEGER not null REFERENCES AUTHORS(id),
    images INTEGER
);


create table AUTHORS(
    id INTEGER PRIMARY KEY AUTOINCREMENT,
    nom varchar(250) not null,
    prenom varchar(250) not null
);


create table COMMENTS(
    id INTEGER PRIMARY KEY AUTOINCREMENT,
    authorId INTEGER not null REFERENCES AUTHORS(id),
    postId INTEGER not null REFERENCES POSTS(id),
    date date not null,
    content TEXT,
    points int default 0
);


create table TAGS(
     id INTEGER PRIMARY KEY AUTOINCREMENT,
     nom varchar(250) UNIQUE
);


create table posts_tags_tags
(
    tagsId  INTEGER not null REFERENCES TAGS (id),
    postsId INTEGER not null REFERENCES POSTS (id),
    PRIMARY KEY (tagsId, postsId)
);